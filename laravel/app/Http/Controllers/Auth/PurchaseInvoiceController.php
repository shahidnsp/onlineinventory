<?php

namespace App\Http\Controllers\Auth;

use App\Purchase_invoice;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class PurchaseInvoiceController extends Controller
{
    /**
     * Validate given request
     * @param array $data
     * @return mixed
     *
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'ledger_id'=>'required',
            'customer_id' =>'required',
            'glaccount_id'=>'required',
            'invoiceno'=>'required',
            'grossamount'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Purchase_invoice::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }
        $purchaseinvoice = new Purchase_invoice($request->all());
        if($purchaseinvoice->save()){
            return $purchaseinvoice;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatore = $this->validator($request->all());
        if($validatore->fails()){
            return Response::json($validatore->errors()
                ,400);
        }

        $purchaseinvoice = Purchase_invoice::find($id);
        if($purchaseinvoice != null)
        {
            $purchaseinvoice->fill($request->all());
            if($purchaseinvoice->save()) {
                return $purchaseinvoice;
            }
        } else{
            return Response::json( ['error' => 'No record found']
                ,500);
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Purchase_invoice::destroy($id))
            return Response::json(array('msg'=>'Purchase Invoice details deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
