<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash_receipt extends Model
{
    protected $fillable = ['ledger_id','date','account_id','amount','cashdiscount','narration','voucherno'];
   public function ledger()
   {
       return $this->belongsTo('App\Ledger');
   }
    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
