<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected  $table='employee';

    protected $fillable = ['empcode','category_id','name','address','place','post','district','phone','mobile','email','website','departement_id','designation','joindate','relivedate','isactive','photo_id'];

    public function category()
    {
        return $this->belongsTo('App\Employee_category');
    }

    public function department()
    {
        return $this->belongsTo('App\Department','departement_id');
    }
    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }
    public  function basic_salary()
    {
        return $this->hasMany('App\Employee_basicsalary');
    }
    public function experiance()
    {
        return $this->hasMany('App\Employee_experiance');
    }
    public function salary()
    {
        return $this->hasMany('App\Employee_salary');
    }
    public function qualification()
    {
        return $this->hasMany('App\Employee_qualification');
    }
    public function leave()
    {
        return $this->hasMany('App\Employee_leaves');
    }
    public function advance()
    {
        return $this->hasMany('App\Employee_advance');
    }
    public function remuneration()
    {
        return $this->hasMany('App\Remuneration');
    }
}
