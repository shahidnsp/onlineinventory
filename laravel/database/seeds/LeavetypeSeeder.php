<?php

use Illuminate\Database\Seeder;

class LeavetypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leavetype=factory(App\Leavetype::class,10)->create();
    }
}
