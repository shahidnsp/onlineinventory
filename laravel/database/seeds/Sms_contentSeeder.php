<?php

use Illuminate\Database\Seeder;

class Sms_contentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $smscontent=factory(App\Sms_content::class,10)->create();
    }
}
