<?php

namespace App\Http\Controllers\Auth;

use App\Salesinvoice;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class SalesInvoiceController extends Controller
{
    /**
     * Validate given request
     * @param array $data
     * @return mixed
     *
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'ledger_id'=>'required',
            'customer_id' =>'required',
            'glaccount_id'=>'required',
            'invoiceno'=>'required',
            'totalamount'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Salesinvoice::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }
        $salesinvoice = new Salesinvoice($request->all());
        if($salesinvoice->save()){
            return $salesinvoice;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatore = $this->validator($request->all());
        if($validatore->fails()){
            return Response::json($validatore->errors()
                ,400);
        }

        $salesinvoice = Salesinvoice::find($id);
        if($salesinvoice != null)
        {
            $salesinvoice->fill($request->all());
            if($salesinvoice->save()) {
                return $salesinvoice;
            }
        } else{
            return Response::json( ['error' => 'No record found']
                ,500);
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Salesinvoice::destroy($id))
            return Response::json(array('msg'=>'Sales Invoice details deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
