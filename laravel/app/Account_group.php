<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account_group extends Model
{
    protected $fillable =['name','under'];
    public function account()
    {
        return $this->hasMany('App\Account');
    }
}
