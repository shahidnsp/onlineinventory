<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable=['code','name','address','tin','cst','servicetax','city','pincode','mobile','phone','email',
        'website','openbalance','closingbalance','creditlimit','creditlimitday','photo_id'];

    public function salesinvoise()
    {
        return $this->hasMany('App\Salesinvoice');
    }


    public function quotation(){
        return $this->hasMany('App\Quotation');
    }

    public function photo(){
        return $this->belongsTo('App\Photo');
    }
}
