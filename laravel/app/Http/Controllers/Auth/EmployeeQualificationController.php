<?php

/**
 * @author Noushid
 */
namespace App\Http\Controllers\Auth;

use App\Employee_qualification;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;


class EmployeeQualificationController extends Controller
{

    /**
     * validate given request
     * @param array $data
     * $return mixed
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'employee_id' => 'required',
            'education' => 'required'
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee_qualification::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
            return Response::json($validator->errors(),500);

        $employeeQualification = new Employee_qualification($request->all());

        if($employeeQualification->save())
            return $employeeQualification;

        return Response::json(['error' => 'Server Down']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employeeQualification = Employee_qualification::find($id);
        if($employeeQualification != null)
            return $employeeQualification;
        else
            return Response::json(['error' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator -> fails())
            return Response::json($validator -> errors(), 500);

        $empQualification = Employee_qualification::find($id);
        if($empQualification != null){

            $empQualification -> fill($request -> all());
            if($empQualification->save())
                return $empQualification;
        }else
            return Response::json(['error' => 'Record not found']);

        return Response::json(['error' => 'Server down!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO employeeQualification model for cascade delete
        if(Employee_qualification::destroy($id))
            return Response::json(['msg' => 'Record deleted']);
        else
            return Response::json(['error' => 'Record not found']);
    }
}
