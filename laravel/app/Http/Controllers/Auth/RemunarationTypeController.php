<?php
/**
 * @author Noushid
 */

namespace App\Http\Controllers\Auth;

use App\Remuneration_type;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class RemunarationTypeController extends Controller
{

    /**
     * validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'type'  => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Remuneration_type::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());

        $remunarationType = new Remuneration_type($request->all());
        if($remunarationType->save())
            return $remunarationType;
        return Response::json(['error' => 'Server error1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $remunarationType = Remuneration_type::find($id);
        if($remunarationType != null)
            return $remunarationType;
        return Response::json(['error' => 'No Record found!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());
        $remunarationType = Remuneration_type::find($id);
        if($remunarationType != null){
            $remunarationType->fill($request->all());
            if($remunarationType->save())
                return $remunarationType;
            else
                return Response::json(['error' => 'Server error!']);
        }else{
            return Response::json(['error' => 'Record not found!']);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Remuneration_type::destroy($id))
            return Response::json(['msg' => 'Record deleted']);
        return Response::json(['error' => 'Record not found']);
    }
}
