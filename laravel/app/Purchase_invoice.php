<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_invoice extends Model
{
    protected $fillable=['invoiceno','invoicedate','deliverydate','ledger_id','vender_id','grossamount',
        'discount','totaltax','netamount','cashdiscount','freightcharge','additional','givenamount','balance',
        'balancepaiddate','note','invoicenote','totalamount','paymentmode','glaccount_id','creditnotification',
        'notiactive'];


    public function purchaceinvoiceitem()
    {
        return $this->hasMany('App\Purchaseinvoice_item');
    }

    public function ledger()
    {
        return $this->belongsTo('App\Ledger');
    }

    public function vender()
    {
        return $this->belongsTo('App\Vender');
    }
}
