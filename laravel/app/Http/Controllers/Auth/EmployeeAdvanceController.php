<?php
/**
 * @author Noushid
 */

namespace App\Http\Controllers\Auth;

use App\Employee_advance;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;
class EmployeeAdvanceController extends Controller
{
    /**
     * validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'employee_id' => 'required',
            'ledger_id' => 'required',
            'amount' => 'required',
            'paymentdate' => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee_advance::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());

        $empAdvance = new Employee_advance($request->all());
        if($empAdvance->save())
            return $empAdvance;

        return Response::json(['error' => 'Server down']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empAdvance = Employee_advance::find($id);
        if($empAdvance != null)
            return $empAdvance;
        else
            return Response::json(['error' => 'Record not found!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());
        $empAdvance = Employee_advance::find($id);
        if($empAdvance != null)
        {
            $empAdvance->fill($request->all());
            if($empAdvance->save())
                return $empAdvance;
            else
                return Response::json(['error' => 'Server Error']);
        }
        else
            return Response::json(['error' => 'Record not found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Employee_advance::destroy($id))
            return Response::json(['msg' => 'Record deleted!']);
        return Response::json(['error' => 'Record Not found!']);
    }
}
