<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable=['quotationno','quotationdate','customer_id','grossamount','discount','totaltax','netamount','cashdiscount',
        'freightamount','additional','note','quotationnote','totalamount'];


    public function customer(){
        return $this->belongsTo('App\Customer');
    }


    public function quotation_item(){
        return $this->hasMany('App\Quotation_items');
    }


}
