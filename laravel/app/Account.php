<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['accountgroup_id', 'accountno', 'accountname','address','contactno'];

    public function account_group()
    {
        return $this->belongsTo('App\Account_group','accountgroup_id');
    }
    public function bank_payment()
    {
        return $this->hasMany('App\Bank_payment');
    }
    public function bank_receipt()
    {
        return $this->hasMany('App\Bank_receipt');
    }
    public function cash_payment()
    {
        return $this->hasMany('App\Cash_payment');
    }
    public function cash_receipt()
    {
        return $this->hasMany('App\Cash_receipt');
    }
}


