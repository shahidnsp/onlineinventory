<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remuneration_type extends Model
{
    protected $fillable = ['type'];
    public function remuneration()
    {
        return $this->hasMany('App\Remuneration','remunarationtype_id');
    }
}
