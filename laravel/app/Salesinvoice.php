<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesinvoice extends Model
{
    protected $fillable=['invoiceno','invoicedate','deliverydate','ledger_id','customer_id','grossamount','discount',
        'totaltax','netamount','cashdiscount','freightcharge','additional','givenamount','balance','balancepaiddate',
        'note','invoicenote','totalamount','paymentmode','glaccount_id','creditnotification','notiactive'];

    public function ledger()
    {
        return $this->belongsTo('App\Ledger');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function salesinvoiceitem()
    {
        return $this->hasMany('App\Salesinvoice_item');
    }

}
