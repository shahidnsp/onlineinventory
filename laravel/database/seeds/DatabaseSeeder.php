<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info = $this->command;
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        \App\User::create([
            'username'=>'admin',
            'password'=>'admin',
            'name'=>'admin',
            'mobile'=>'1234567890',
            'phone'=>'0483123456',
            'email'=>'admin@admin.com',
            'permission'=>'111,111,111,111,111,111,111,111,111,111,111,111,111']);

        $info->comment('Admin user created');
        $info->error('Username : admin Password:admin');

        $info->info('User table seeding started...');
        $this->call('UserSeeder');

        $info->info('Account table seeding started...');
        $this->call('AccountSeeder');

        $info->info('Bank payment Table seeding started....');
        $this->call('Bank_paymentSeeder');

        $info->info('Bank receipt Table seeding started....');
        $this->call('Bank_receiptSeeder');

        $info->info('Cash payament Table seeding started....');
        $this->call('Cash_paymentSeeder');

        $info->info('Cash receipt Table seeding started....');
        $this->call('Cash_receiptSeeder');

        $info->info('Cash receipt Table seeding started....');
        $this->call('Bank_receiptSeeder');

        $info->info('Account group Table seeding started....');
        $this->call('Account_groupSeeder');

        $info->info('Ledger Table seeding started....');
        $this->call('LedgerSeeder');

        $info->info('Ledger amount Table seeding started....');
        $this->call('Ledger_amountSeeder');

        $info->info('Journal amount Table seeding started....');
        $this->call('JournalSeeder');

        $info->info('Sms registery Table seeding started....');
        $this->call('Sms_registerySeeder');

        $info->info('Prefix Table seeding started....');
        $this->call('PrefixSeeder');

        $info->info('Photo Table seeding started....');
        $this->call('PhotoSeeder');

        $info->info('Company profile Table seeding started....');
        $this->call('Company_profileSeeder');

        $info->info('Sms_content Table seeding started....');
        $this->call('Sms_contentSeeder');

        $info->info('Contact Table seeding started....');
        $this->call('ContactSeeder');

        $info->info('Note Table seeding started....');
        $this->call('NoteSeeder');

        $info->info('Reminder Table seeding started....');
        $this->call('ReminderSeeder');

        $info->info('Customer Table seeding started....');
        $this->call('CustomerSeeder');

        $info->info('Salesinvoice item Table seeding started....');
        $this->call('Salesinvoice_itemSeeder');

        $info->info('Sales invoice Table seeding started....');
        $this->call('SalesinvoiceSeeder');

        $info->info('Quotation Table seeding started....');
        $this->call('QuotationSeeder');

        $info->info('Quotation items Table seeding started....');
        $this->call('Quotation_itemSeeder');

        $info->info('Vender Table seeding started....');
        $this->call('VenderSeeder');

        $info->info('Purchase invoice seeding started....');
        $this->call('Purchase_invoiceSeeder');

        $info->info('Purchase invoice item seeding started....');
        $this->call('Purchaseinvoice_itemSeeder');

        $info->info('Purchase order seeding started....');
        $this->call('Purchase_orderSeeder');

        $info->info('Purchase order item seeding started....');
        $this->call('Purchaseorder_itemSeeder');

        $info->info('Employee seeding started....');
        $this->call('EmployeeSeeder');

        $info->info('Depatment seeding started....');
        $this->call('DepartmentSeeder');

        $info->info('Employee experiance seeding started....');
        $this->call('Employee_experianceSeeder');

        $info->info('Employee qualification seeding started....');
        $this->call('Employee_qualificationSeeder');

        $info->info('Employee basic salary seeding started....');
        $this->call('Employee_basicsalrySeeder');

        $info->info('Employee salary seeding started....');
        $this->call('Employee_salrySeeder');

        $info->info('Employee leave seeding started....');
        $this->call('Employee_leaveSeeder');

        $info->info('Employee category seeding started....');
        $this->call('Employee_categorySeeder');

        $info->info('Leavetype seeding started....');
        $this->call('LeavetypeSeeder');

        $info->info('Employee advance seeding started....');
        $this->call('Employee_advanceSeeder');

        $info->info('Remuneration seeding started....');
        $this->call('RemunerationSeeder');

        $info->info('Remuneration type seeding started....');
        $this->call('Remuneration_typeSeeder');

        $info->info('Driver seeding started....');
        $this->call('DriverSeeder');

        $info->info('Vehicle seeding started....');
        $this->call('VehicleSeeder');

        $info->info('Inventory item seeding started....');
        $this->call('Inventory_itemSeeder');

        $info->info('Item seeding started....');
        $this->call('ItemSeeder');

        $info->info('Item price seeding started....');
        $this->call('Item_priceSeeder');

        $info->info('Unit seeding started....');
        $this->call('UnitSeeder');

        $info->info('Taxe seeding started....');
        $this->call('TaxeSeeder');

        $info->info('Location seeding started....');
        $this->call('LocationSeeder');

        $info->info('Sub category seeding started....');
        $this->call('SubcategorySeeder');

        $info->info('Main category seeding started....');
        $this->call('MaincategorySeeder');

        Model::reguard();
    }
}
