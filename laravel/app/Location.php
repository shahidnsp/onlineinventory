<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable=['name'];
    public function inventory_item()
    {
        return $this->hasMany('App\Inventory_item');
    }
}
