<?php

use Illuminate\Database\Seeder;

class Quotation_itemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quotation_items=factory(App\Quotation_items::class,10)->create();
    }
}
