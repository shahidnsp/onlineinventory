<?php

namespace App\Http\Controllers\Auth;

use App\Ledger_amount;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;


class LedgerAmountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'ledger_id'=>'required',
            'amount'=>'required',
            'opendate'=>'required',
        ]);
    }
    public function index()
    {
        return Ledger_amount::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $ledgeramnt = new Ledger_amount($request->all());

        $ledgeramnt->fill($request->all());
        if($ledgeramnt->save())
        {
            return $ledgeramnt;
        }
        else
            return Response::json( ['error' => 'Server is down']
                ,500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $ledgeramnt = Ledger_amount::find($id);
        if($ledgeramnt != null)
        {
            $ledgeramnt->fill($request->all());
            if ($ledgeramnt->save()) {
                return $ledgeramnt;
            } else
                return Response::json(['error' => 'Server is down'], 500);
        }
        else
            return Response::json(['error'=>'Record not found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Account model for cascade delete

        if(Ledger_amount::destroy($id))
            return Response::json(array('msg'=>'Banks Account deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
