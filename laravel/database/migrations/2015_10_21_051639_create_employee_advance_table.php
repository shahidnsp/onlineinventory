<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAdvanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_advances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('ledger_id');
            $table->string('amount');
            $table->string('description');
            $table->string('paymentdate');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_advances');
    }
}
