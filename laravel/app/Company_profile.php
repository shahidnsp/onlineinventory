<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_profile extends Model
{
    protected $fillable = ['name','addresss', 'phone','mobile', 'email', 'website', 'cst', 'bst', 'tin', 'branch', 'regno', 'photo_id'];

    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }
}
