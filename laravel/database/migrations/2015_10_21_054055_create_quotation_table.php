<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quotationno');
            $table->string('quotationdate');
            $table->integer('customer_id');
            $table->string('grossamount');
            $table->string('discount');
            $table->string('totaltax');
            $table->string('netamount');
            $table->string('cashdiscount');
            $table->string('freightamount');
            $table->string('additional');
            $table->string('note');
            $table->string('quotationnote');
            $table->string('totalamount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotations');
    }
}
