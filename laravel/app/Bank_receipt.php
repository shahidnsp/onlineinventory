<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank_receipt extends Model
{
    protected $fillable = ['account_id','datebanked','reference','narration'];
    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
