<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms_registary extends Model
{
    protected $table='sms_registaries';

    protected $fillable = ['username', 'password', 'sender_id'];
}
