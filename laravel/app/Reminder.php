<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected  $fillable=['description','notifydate','time','isactieve','taskdate'];
}
