<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchaseinvoice_item extends Model
{
    protected $fillable=['invoice_id','item_id','rate','discount','qty','unit_id','tax_id','net','mrp'];


    public function purchaceinvoice()
    {
        return $this->belongsTo('App\Purchase_invoice');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function tax()
    {
        return $this->belongsTo('App\Tax');
    }
}
