<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paymentdate');
            $table->string('debitaccount');
            $table->string('creditaccount');
            $table->string('debitamount');
            $table->string('creditamount');
            $table->string('ledgerfolio');
            $table->string('description');
            $table->string('vouchertype');
            $table->string('sales');
            $table->string('purchase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journals');
    }
}
