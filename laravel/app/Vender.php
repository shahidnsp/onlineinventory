<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vender extends Model
{

    protected $fillable=['suppliercode','name','shortname','address1','address2','city','pincode','phone','mobile',
        'email','website','openbalance','closingbalance','discount','creditlimit','overduedays','cstno','bstno',
        'kgst','regino','photo_id'];



    public function item()
    {
        return $this->hasMany('App\Item','vendor_id');
    }

    public function purchaceorder()
    {
        return $this->hasMany('App\Purchase_order');
    }

    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    public function purchaseinvoice()
    {
        return $this->hasMany('App\Purchase_invoice');
    }
}
