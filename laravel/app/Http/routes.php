<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('test', function () {

    return \App\Company_profile::with('photo')->get();

});






Route::group(['prefix' => 'api'], function () {

    //Shahid...................................................

    Route::resource('account', 'Auth\AccountController');
    Route::resource('smsRegistry', 'Auth\SmsRegistryController');
    Route::resource('companyProfile', 'Auth\CompanyProfileController');
    Route::resource('smsContent', 'Auth\SmsContentController');
    Route::resource('photo', 'Auth\PhotoController');
    Route::resource('prefix', 'Auth\PrefixController');
    Route::resource('note', 'Auth\NoteController');

    Route::resource('reminder','Auth\ReminderController');
    Route::resource('todo','Auth\NoteController');

    Route::resource('item','Auth\ItemController');
    Route::resource('tax','Auth\TaxController');
    Route::resource('inventoryItem','Auth\InventoryItemController');
    Route::resource('itemPrice','Auth\ItemPriceController');
    Route::resource('location','Auth\LocationController');
    Route::resource('unit','Auth\UnitController');
    Route::resource('subcategory','Auth\SubCategoryController');
    Route::resource('maincategory','Auth\MainCategoryController');

    //....................................................Shahid


    //Shammas...................................................

    Route::resource('customer', 'Auth\CustomerController');
    Route::resource('salesInvoiceItem', 'Auth\SalesInvoiceItemController');
    Route::resource('salesInvoice', 'Auth\SalesInvoiceController');
    Route::resource('quotation', 'Auth\QuotationController');
    Route::resource('quotationItem', 'Auth\QuotationItemController');
    Route::resource('vendor', 'Auth\VendorController');
    Route::resource('purchaseInvoice', 'Auth\PurchaseInvoiceController');
    Route::resource('purchaseInvoiceItem', 'Auth\PurchaseInvoiceItemController');
    Route::resource('purchaseOrder','Auth\PurchaseOrderController');
    Route::resource('purchaseOrderItem','Auth\PurchaseOrderItemController');

    //...................................................Shammas

    //Noushid...................................................

    Route::resource('employee','Auth\EmployeeController');
    Route::resource('department','Auth\DepartmentController');
    Route::resource('empExperience','Auth\EmployeeExperianceController');
    Route::resource('empQualification', 'Auth\EmployeeQualificationController');
    Route::resource('empBasicSalary', 'Auth\EmployeeBasicSalaryController');
    Route::resource('empSalary', 'Auth\EmployeeSalaryController');
    Route::resource('empLeave', 'Auth\EmployeeLeaveController');
    Route::resource('empLeaveType', 'Auth\EmployeeLeaveTypeController');
    Route::resource('empAdvance', 'Auth\EmployeeAdvanceController');
    Route::resource('empRemuneration', 'Auth\RemunarationController');
    Route::resource('empRemunerationType', 'Auth\RemunarationTypeController');
    Route::resource('driver', 'Auth\DriverController');
    Route::resource('vehicle', 'Auth\VehicleController');


    //...................................................Noushid

    //Naseeba...................................................

    Route::resource('reminder','Auth\ReminderController');
    Route::resource('bankpayment','Auth\BankPaymentController');
    Route::resource('bankreceipt','Auth\BankReceiptController');
    Route::resource('cashpayment','Auth\CashPaymentController');
    Route::resource('cashreceipt','Auth\CashReceiptController');
    Route::resource('accountgroup','Auth\AccountGroupController');
    Route::resource('ledger','Auth\LedgerController');
    Route::resource('ledgeramount','Auth\LedgerAmountController');
    Route::resource('journal','Auth\JournalController');

    //....................................................Naseeba

});



//Shahid...........................................................

//Load angular templates
//TODO user permission validation
Route::get('template/{name}', ['as' => 'template', function ($name) {
//    $permission = UserHelper::pages(Auth::user()->permission,false,true,true);
//    return view('app.' . $name,['permission'=>$permission]);
    return view('app.' . $name);
}]);

//TODO load temp test json files
Route::get('json/{name}', function ($name) {

    return Storage::get('json/' . $name);
});

// Catch all undefined routes and give to index.php to handle  .
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('app.index');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');


// Using different syntax for Blade to avoid conflicts with Jade.
// You are well-advised to go without any Blade at all.
Blade::setContentTags('[[', ']]');
// For variables and all things Blade.
Blade::setEscapedContentTags('[[-', '-]]');
// For escaped data.

//..............................................................Shahid
