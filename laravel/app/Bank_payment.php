<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank_payment extends Model
{
    protected $fillable = ['paymentdate','account_id','chequeno','transactionno','narration'];
    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
