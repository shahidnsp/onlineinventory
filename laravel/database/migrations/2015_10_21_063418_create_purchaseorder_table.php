<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vender_id');
            $table->string('grossamount');
            $table->string('discount');
            $table->string('totaltax');
            $table->string('netamount');
            $table->string('cashdiscount');
            $table->string('additional');
            $table->string('totalamount');
            $table->string('paid');
            $table->string('balance');
            $table->string('invoicedate');
            $table->string('deliverydate');
            $table->string('note');
            $table->string('ordernote');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_orders');
    }
}
