<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemunerationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remunerations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('remunarationtype_id');
            $table->integer('ledger_id');
            $table->integer('employee_id');
            $table->string('name');
            $table->string('amount');
            $table->string('description');
            $table->string('paymentdate');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('remunerations');
    }
}
