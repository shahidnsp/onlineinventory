<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_experiance extends Model
{
    protected $fillable =
        [
            'employee_id','designation','firm','year'
        ];
    public function employee(){
        return $this->belongsTo('App\Employee');
    }
}
