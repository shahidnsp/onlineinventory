<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salarys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('presentday');
            $table->string('education');
            $table->string('overtime');
            $table->string('overtimeamount');
            $table->string('paymentdate');
            $table->string('paymentmode');
            $table->string('overtimerate');
            $table->string('description');
            $table->decimal('total',18,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_salarys');
    }
}
