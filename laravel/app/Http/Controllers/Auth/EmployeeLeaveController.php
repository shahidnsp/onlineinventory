<?php

/**
 * @author noushid
 */
namespace App\Http\Controllers\Auth;

use App\Employee_leaves;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class EmployeeLeaveController extends Controller
{
    /**
     * validate given request
     * @param array $data
     * @return mixed
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'employee_id' => 'required',
            'leavetype_id' => 'required',
            'leavedate' => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee_leaves::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
            return Response::json($validator->errors(),500);

        $empLeave = new Employee_leaves($request->all());
        if($empLeave->save())
            return $empLeave;
        return Response::json(['error' => 'Server error!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empLeave = Employee_leaves::find($id);
        if($empLeave != null)
            return $empLeave;
        return Response::json(['error' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = $this->validator($request->all());
        if($validator -> fails())
            return Response::json($validator->errors(),500);

        $empLeave = Employee_leaves::find($id);
        if($empLeave != null){
            $empLeave->fill($request->all());
            if($empLeave->save())
                return $empLeave;
            else
                return Response::json(['error' => 'Server down!']);
        }else{
            return Response::json(['error' => 'Record not found']);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Employee_leaves::destroy($id))
            return Response::json(['msg' => 'Record deleted']);
        return Response::json(['error' => 'Record Not found!']);

    }
}
