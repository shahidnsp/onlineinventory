<?php

namespace App\Http\Controllers\Auth;

/**
 *@author Noushid
 */
use App\Employee_basicsalary;
use App\Employee_salary;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class EmployeeSalaryController extends Controller
{
    /**
     *validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'employee_id' => 'required',
            'presentday' => 'required',
            'total' => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee_salary::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator -> fails())
            return Response::json($validator->errors(), 500);

        $empSlry = new Employee_salary($request->all());

        if($empSlry->save())
            return $empSlry;

        return Response::json(['error' => 'Server down'], 400);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empSalary = Employee_salary::find($id);
        if($empSalary != null)
            return$empSalary;
        else
            return Response::json(['error' => 'Not found']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
            return Response::json($validator->errors());

        $empSalary = Employee_salary::find($id);
        if($empSalary != null){
            $empSalary->fill($request->all());
            if($empSalary->save())
                return $empSalary;
            else{
                return Response::json(['error' => 'Server down!']);
            }
        }else{

            return Response::json(['error' => 'Record not found']);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Employee_salary::destroy($id))
            return Response::json(['msg' => 'Record deleted']);
        else
            return Response::json(['erroe' => 'Record Not found!']);

    }
}
