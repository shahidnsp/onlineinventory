<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prefix extends Model
{
    protected $fillable = ['prefix','yes','type','year'];
}
