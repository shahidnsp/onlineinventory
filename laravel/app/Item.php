<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected  $fillable=['code','barcode','name','unit_id','discription','size','tax_id','subcategory_id','maincatecory_id','color','vendor_id','isactive','type','photo_id'];
    public function inventory_item()
    {
        return $this->hasMany('App\Inventory_item');
    }
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }
    public function tax()
    {
        return $this->belongsTo('App\Taxe');
    }
    public function sub_category()
    {
        return $this->belongsTo('App\Subcategory','subcategory_id');
    }
    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }
    public function main_category()
    {
        return $this->belongsTo('App\Maincategory','maincatecory_id');
    }
    public function vendor()
    {
        return $this->belongsTo('App\Vender');
    }


    public function purchaceorderitems()
    {
        return $this->hasMany('App\Purchaseorder_items');
    }

    public function salesinvoiceitem()
    {
        return $this->hasMany('App\Salesinvoice_item');
    }

    public function quotation_item()
    {
        return $this->hasMany('App\Quotation_items');

    }
}
