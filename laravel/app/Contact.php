<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $fillable = ['firstname','lastname','address','city','district','state','pin','phone','mobile','email1','email2','website','photo_id','type'];

    public function photo()
    {
        return $this->belongsTo('App\Photo','photo_id');
    }

}
