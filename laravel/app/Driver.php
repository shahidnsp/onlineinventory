<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [ 'name', 'address', 'mobile', 'vehicle_id' ];
    public function vehicle()
    {
        return $this->belongsTo('App/Vehicle');
    }
}
