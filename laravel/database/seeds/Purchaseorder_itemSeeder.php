<?php

use Illuminate\Database\Seeder;

class Purchaseorder_itemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchaseorder_item=factory(App\Purchaseorder_items::class,10)->create();
    }
}
