<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [ 'name', 'regnumber', 'manufacture', 'color', 'rcowner', 'contact', 'servicedate'];

    public function type()
    {
        return $this->hasMany('App\Driver');
    }
}
