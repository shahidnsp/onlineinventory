<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_basicsalary extends Model
{

    protected $table='employee_basicsalarys';
    protected $fillable = ['employee_id', 'salary','effectdate'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
