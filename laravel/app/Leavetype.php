<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leavetype extends Model
{
    protected $fillable = ['name', 'alloweddays'];
    public function employee_leave()
    {
        return $this->hasMany('App\Employee_leaves');
    }
}
