<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    protected $fillable = ['name','type','classification','realornominal','depreciation'];
    public function employee_advance()
    {
        return $this->hasMany('App\Employee_advance');
    }
    public function remuneration()
    {
        return $this->hasMany('App\Remuneration');
    }
    public function cash_payment()
    {
        return $this->hasMany('App\Cash_payment');
    }
    public function ledger_amount()
    {
        return $this->hasMany('App\Ledger_amount');
    }
    public function cash_receipt()
    {
        return $this->hasMany('App\Cash_receipt');
    }

    public function purchaceinvoice()
    {
        return $this->hasMany('App\Purchase_invoice');
    }

    public function salesinvoice()
    {
        return $this->hasMany('App\Salesinvoice');
    }

}
