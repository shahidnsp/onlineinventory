<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable=['name','isunit','kg'];

    public function item()
    {
        return $this->hasMany('App\Item');
    }


    public function purchaceorderitems()
    {
        return $this->hasMany('App\Unit');
    }

    public function salesinvoiceunit()
    {
        return $this->hasMany('App\Salesinvoice_item');
    }


    public function quotation_item(){
        return $this->hasMany('App\Quotation_items');

    }
}
