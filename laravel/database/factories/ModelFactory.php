<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->name,
        'password' => bcrypt(str_random(10)),
        'name'=>$faker->name,
        'mobile'=>$faker->phoneNumber,
        'phone'=>$faker->phoneNumber,
        'email' => $faker->email,
        'photo_id'=>$faker->randomElement([1,2,3,4,5]),
        'remember_token' => str_random(10),
    ];
});

//Account factory
$factory->define(App\Account::class, function (Faker\Generator $faker) {
    return [
        'accountgroup_id' => $faker->randomElement([1,2,3,4,5,6,7,8]),
        'accountno' => $faker->creditCardNumber,
        'accountname' =>$faker->name,
        'address'=>$faker->address,
        'contactno'=>$faker->phoneNumber,
    ];
});


//Bank Payment factory
$factory->define(App\Bank_payment::class, function (Faker\Generator $faker) {
    return [
        'paymentdate' =>$faker->date,
        'account_id'  =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'chequeno'    =>$faker->numberBetween(1,20),
        'transactionno'=>$faker->currencyCode,
        'narration'   =>$faker->sentence,
    ];
});

//Bank receipt factory
$factory->define(App\Bank_receipt::class, function (Faker\Generator $faker) {
    return [
        'account_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'datebanked'=>$faker->date,
        'reference'=>$faker->currencyCode,
        'narration'=>$faker->sentence,
    ];
});

//Cash payment factory
$factory->define(App\Cash_payment::class, function (Faker\Generator $faker) {
    return [
        'ledger_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'date'=>$faker->date,
        'account_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>$faker->randomDigitNotNull,
        'cashdiscount'=>$faker->randomDigit,
        'narration'=>$faker->sentence,
        'voucherno'=>$faker->numberBetween(1,30),

    ];
});

//Cash reciept factory
$factory->define(App\Cash_receipt::class, function (Faker\Generator $faker) {
    return [
        'ledger_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'date'=>$faker->date,
        'account_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>$faker->randomNumber,
        'cashdiscount'=>$faker->randomDigit,
        'narration'=>$faker->sentence,
        'voucherno'=>$faker->numberBetween(1,30),

    ];
});

//Account group factory
$factory->define(App\Account_group::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'under'=>$faker->name,
    ];
});

//Ledger factory
$factory->define(App\Ledger::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'type'=>$faker->lastName,
        'classification'=>$faker->name,
        'realornominal'=>$faker->randomElement(['real','nominal']),
        'depreciation'=>$faker->numberBetween(0,100),

    ];
});

//Ledger amount factory
$factory->define(App\Ledger_amount::class, function (Faker\Generator $faker) {
    return [
        'ledger_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>$faker->randomNumber,
        'opendate'=>$faker->date,
        'closedate'=>$faker->date,

    ];
});

//Journal amount factory
$factory->define(App\Journal::class, function (Faker\Generator $faker) {
    return [
        'paymentdate'=>$faker->date,
        'debitaccount'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'creditaccount'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'debitamount'=>$faker->randomNumber,
        'creditamount'=>$faker->randomNumber,
        'ledgerfolio'=>$faker->randomDigit,
        'description'=>$faker->sentence,
        'vouchertype'=>$faker->randomElement(['cashreciept','salesinvoice','purchaseinvoice','cashpayment']),
        'sales'=>$faker->randomNumber,
        'purchase'=>$faker->randomNumber,

    ];
});

//Sms registery amount factory
$factory->define(App\Sms_registary::class, function (Faker\Generator $faker) {
    return [
        'username'=>$faker->userName,
        'password'=>$faker->password,
        'sender_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),

    ];
});

//Prefix factory
$factory->define(App\Prefix::class, function (Faker\Generator $faker) {
    return [
        'prefix'=>$faker->sentence,
        'yes'=>$faker->randomElement(['1','0']),
        'type'=>$faker->randomElement(['sales','purchase','customer','employee','qoutation','order','vender']),
        'year'=>$faker->year,

    ];
});

//Photo factory
$factory->define(App\Photo::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
    ];
});

//Company profile factory
$factory->define(App\Company_profile::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'addresss'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'website'=>$faker->domainName,
        'cst'=>$faker->creditCardNumber,
        'bst'=>$faker->creditCardNumber,
        'tin'=>$faker->randomDigit,
        'branch'=>$faker->streetName,
        'regno'=>$faker->randomDigit,
        'photo_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Sms content factory
$factory->define(App\Sms_content::class, function (Faker\Generator $faker) {
    return [
        'content'=>$faker->sentence,
    ];
});

//Contact factory
$factory->define(App\Contact::class, function (Faker\Generator $faker) {
    return [
        'firstname'=>$faker->firstName,
        'lastname'=>$faker->lastName,
        'address'=>$faker->address,
        'city'=>$faker->city,
        'district'=>$faker->streetName,
        'state'=>$faker->state,
        'pin'=>$faker->postcode,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'email1'=>$faker->email,
        'email2'=>$faker->email,
        'website'=>$faker->domainName,
        'photo_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'type'=>$faker->randomElement(['customer','vendor','other'])

    ];
});

//Note content factory
$factory->define(App\Note::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'description'=>$faker->sentence,
    ];
});

//Reminder content factory
$factory->define(App\Reminder::class, function (Faker\Generator $faker) {
    return [
        'description'=>$faker->sentence,
        'notifydate'=>$faker->date,
        'time'=>$faker->time,
        'isactieve'=>$faker->randomElement(['1','0']),
        'taskdate'=>$faker->date,
    ];
});

//Customer factory
$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'code'=>$faker->randomDigit,
        'name'=>$faker->name,
        'address'=>$faker->address,
        'tin'=>$faker->randomNumber,
        'cst'=>$faker->randomNumber,
        'servicetax'=>$faker->randomNumber,
        'city'=>$faker->city,
        'pincode'=>$faker->postcode,
        'mobile'=>$faker->phoneNumber,
        'phone'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'website'=>$faker->domainName,
        'openbalance'=>$faker->randomNumber,
        'closingbalance'=>0,
        'creditlimit'=>100,
        'creditlimitday'=>30,
        'photo_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Salesinvoice items factory
$factory->define(App\Salesinvoice_item::class, function (Faker\Generator $faker) {
    return [
        'salesinvoice_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'item_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'rate'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'qty'=>$faker->numberBetween(1,100),
        'unit_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'tax_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'net'=>$faker->randomNumber,
        'mrp'=>$faker->randomNumber,

    ];
});

//Sales invoice factory
$factory->define(App\Salesinvoice::class, function (Faker\Generator $faker) {
    return [
        'invoiceno'=>$faker->randomNumber,
        'invoicedate'=>$faker->date,
        'deliverydate'=>$faker->date,
        'ledger_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'customer_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'grossamount'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'totaltax'=>$faker->randomDigit,
        'netamount'=>$faker->randomNumber,
        'cashdiscount'=>$faker->randomDigit,
        'freightcharge'=>$faker->randomDigit,
        'additional'=>$faker->randomDigit,
        'givenamount'=>$faker->randomNumber,
        'balance'=>$faker->randomDigit,
        'balancepaiddate'=>$faker->date,
        'note'=>$faker->sentence,
        'invoicenote'=>$faker->sentence,
        'totalamount'=>$faker->randomNumber,
        'paymentmode'=>$faker->randomElement(['Dr','Cr']),
        'glaccount_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'creditnotification'=>$faker->sentence,
        'notiactive'=>$faker->randomElement(['1','0']),

    ];
});

//Quotation factory
$factory->define(App\Quotation::class, function (Faker\Generator $faker) {
    return [
        'quotationno'=>$faker->randomDigit,
        'quotationdate'=>$faker->date,
        'customer_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'grossamount'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'totaltax'=>$faker->randomDigit,
        'netamount'=>$faker->randomNumber,
        'cashdiscount'=>$faker->randomDigit,
        'freightamount'=>$faker->randomDigit,
        'additional'=>$faker->randomDigit,
        'note'=>$faker->sentence,
        'quotationnote'=>$faker->sentence,
        'totalamount'=>$faker->randomNumber,

    ];
});

//Quotation_items factory
$factory->define(App\Quotation_items::class, function (Faker\Generator $faker) {
    return [
        'quotation_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'item_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'rate'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'qty'=>$faker->randomDigit,
        'unit_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'tax_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'net'=>$faker->randomNumber,
        'mrp'=>$faker->randomNumber,

    ];
});

//Vender factory
$factory->define(App\Vender::class, function (Faker\Generator $faker) {
    return [
        'suppliercode'=>$faker->randomNumber,
        'name'=>$faker->name,
        'shortname'=>$faker->lastName,
        'address1'=>$faker->address,
        'address2'=>$faker->address,
        'city'=>$faker->city,
        'pincode'=>$faker->postcode,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'website'=>$faker->domainName,
        'openbalance'=>1000,
        'closingbalance'=>0,
        'discount'=>$faker->randomDigit,
        'creditlimit'=>10000,
        'overduedays'=>$faker->randomDigit,
        'cstno'=>$faker->randomNumber,
        'bstno'=>$faker->randomNumber,
        'kgst'=>$faker->randomNumber,
        'regino'=>$faker->randomNumber,
        'photo_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),

    ];
});

//Purchase invoice factory
$factory->define(App\Purchase_invoice::class, function (Faker\Generator $faker) {
    return [
        'invoiceno'=>$faker->randomNumber,
        'invoicedate'=>$faker->date,
        'deliverydate'=>$faker->date,
        'ledger_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'vender_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'grossamount'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'totaltax'=>$faker->randomDigit,
        'netamount'=>$faker->randomNumber,
        'cashdiscount'=>$faker->randomDigit,
        'freightcharge'=>$faker->randomDigit,
        'additional'=>$faker->randomDigit,
        'givenamount'=>$faker->randomNumber,
        'balance'=>$faker->randomDigit,
        'balancepaiddate'=>$faker->date,
        'note'=>$faker->sentence,
        'invoicenote'=>$faker->sentence,
        'totalamount'=>$faker->randomNumber,
        'paymentmode'=>$faker->randomElement(['Dr','Cr']),
        'glaccount_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'creditnotification'=>$faker->sentence,
        'notiactive'=>$faker->randomElement(['1','0']),

    ];
});

//Purchase invoice item factory
$factory->define(App\Purchaseinvoice_item::class, function (Faker\Generator $faker) {
    return [
        'invoice_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'item_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'rate'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'qty'=>$faker->numberBetween(1,100),
        'unit_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'tax_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'net'=>$faker->randomNumber,
        'mrp'=>$faker->randomNumber,
    ];
});

//Purchase order factory
$factory->define(App\Purchase_order::class, function (Faker\Generator $faker) {
    return [
        'vender_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'grossamount'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'totaltax'=>$faker->randomDigit,
        'netamount'=>$faker->randomNumber,
        'cashdiscount'=>$faker->randomDigit,
        'additional'=>$faker->randomDigit,
        'totalamount'=>$faker->randomNumber,
        'paid'=>$faker->randomDigit,
        'balance'=>$faker->randomDigit,
        'invoicedate'=>$faker->date,
        'deliverydate'=>$faker->date,
        'note'=>$faker->sentence,
        'ordernote'=>$faker->sentence,
    ];
});

//Purchase order item factory
$factory->define(App\Purchaseorder_items::class, function (Faker\Generator $faker) {
    return [
        'purchaseorder_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'item_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'rate'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'qty'=>$faker->numberBetween(1,100),
        'unit_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'tax_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'net'=>$faker->randomDigit,
        'mrp'=>$faker->randomDigit,

    ];
});

//Employee factory
$factory->define(App\Employee::class, function (Faker\Generator $faker) {
    return [
        'empcode'=>$faker->randomNumber,
        'category_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'name'=>$faker->name,
        'address'=>$faker->address,
        'place'=>$faker->streetName,
        'post'=>$faker->streetName,
        'district'=>$faker->state,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'website'=>$faker->domainName,
        'departement_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'designation'=>$faker->randomElement(['slesman','superviser','cashier','sweaper']),
        'joindate'=>$faker->date,
        'relivedate'=>$faker->date,
        'isactive'=>$faker->randomElement([1,0]),
        'photo_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),


    ];
});

//Department factory
$factory->define(App\Department::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'phone'=>$faker->phoneNumber,
        'hod'=>$faker->name,
    ];
});

//Employee experiance factory
$factory->define(App\Employee_experiance::class, function (Faker\Generator $faker) {
    return [
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'designation'=>$faker->randomElement(['slesman','superviser','cashier','sweaper']),
        'firm'=>$faker->randomElement(['IT','bba','bca','bsc']),
        'year'=>$faker->year,
    ];
});

//Employee qualification factory
$factory->define(App\Employee_qualification::class, function (Faker\Generator $faker) {
    return [
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'education'=>$faker->randomElement(['IT','bba','bca','bsc']),
        'college'=>$faker->companySuffix,
        'mark'=>$faker->randomDigit,
    ];
});

//Employee basic salary factory
$factory->define(App\Employee_basicsalary::class, function (Faker\Generator $faker) {
    return [
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'salary'=>$faker->randomNumber,
        'effectdate'=>$faker->date,

    ];
});

//Employee salary factory
$factory->define(App\Employee_salary::class, function (Faker\Generator $faker) {
    return [
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'presentday'=>$faker->dayOfWeek,
        'education'=>$faker->randomElement(['IT','bba','bca','bsc']),
        'overtime'=>$faker->time,
        'overtimeamount'=>$faker->randomNumber,
        'paymentdate'=>$faker->date,
        'paymentmode'=>$faker->randomElement(['Dr','Cr']),
        'overtimerate'=>$faker->randomDigit,
        'description'=>$faker->sentence,
        'total'=>$faker->randomNumber,

    ];
});

//Employee leave factory
$factory->define(App\Employee_leaves::class, function (Faker\Generator $faker) {
    return [
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'leavetype_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'leavedate'=>$faker->date,
        'time'=>$faker->time,
    ];
});

//Employee category factory
$factory->define(App\Employee_category::class, function (Faker\Generator $faker) {
    return [
        'type'=>$faker->randomElement(['slesman','superviser','cashier','sweaper']),
    ];
});

//Leave type factory
$factory->define(App\Leavetype::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'alloweddays'=>$faker->randomDigit,
    ];
});

//Employee advance factory
$factory->define(App\Employee_advance::class, function (Faker\Generator $faker) {
    return [
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'ledger_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>$faker->randomNumber,
        'description'=>$faker->sentence,
        'paymentdate'=>$faker->date,
    ];
});

//Remuneration factory
$factory->define(App\Remuneration::class, function (Faker\Generator $faker) {
    return [
        'remunarationtype_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'ledger_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'name'=>$faker->name,
        'amount'=>$faker->randomNumber,
        'description'=>$faker->sentence,
        'paymentdate'=>$faker->date,

    ];
});

//Remuneration type factory
$factory->define(App\Remuneration_type::class, function (Faker\Generator $faker) {
    return [
        'type'=>$faker->randomElement(['Free','allovence','TA','DA']),
    ];
});


//Driver factory
$factory->define(App\Driver::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'address'=>$faker->address,
        'mobile'=>$faker->phoneNumber,
        'vehicle_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Vehicle factory
$factory->define(App\Vehicle::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'regnumber'=>$faker->randomDigit,
        'manufacture'=>$faker->name,
        'color'=>$faker->colorName,
        'rcowner'=>$faker->firstName,
        'contact'=>$faker->phoneNumber,
        'servicedate'=>$faker->date,

    ];
});

//Inventory item factory
$factory->define(App\Inventory_item::class, function (Faker\Generator $faker) {
    return [
        'item_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'expirydate'=>$faker->date,
        'batchnumber'=>$faker->randomDigit,
        'packetnumber'=>$faker->randomDigit,
        'location_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'quantity'=>$faker->numberBetween(1,100),
        'warningqty'=>$faker->numberBetween(1,100),

    ];
});

//Item factory
$factory->define(App\Item::class, function (Faker\Generator $faker) {
    return [
        'code'=>$faker->randomNumber,
        'barcode'=>$faker->randomNumber,
        'name'=>$faker->name,
        'unit_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'discription'=>$faker->sentence,
        'size'=>$faker->randomDigit,
        'tax_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'subcategory_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'maincatecory_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'color'=>$faker->colorName,
        'vendor_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'isactive'=>$faker->randomElement([1,0]),
        'type'=>$faker->randomElement(['inventory','service']),
        'photo_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Item price factory
$factory->define(App\Item_price::class, function (Faker\Generator $faker) {
    return [
        'inventory_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'salesrate'=>$faker->randomNumber,
        'purchaserate'=>$faker->randomNumber,
        'discount'=>$faker->randomDigit,
        'effectdate'=>$faker->date,

    ];
});

//Unit factory
$factory->define(App\Unit::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'isunit'=>$faker->randomElement([1,0]),
        'kg'=>$faker->numberBetween(0,1000),

    ];
});

//Taxe factory
$factory->define(App\Taxe::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'percentage'=>$faker->numberBetween(0,100),

    ];
});


//Location factory
$factory->define(App\Location::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
    ];
});


//Subcategory factory
$factory->define(App\Subcategory::class, function (Faker\Generator $faker) {
    return [
        'maincategory_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'name'=>$faker->name,
    ];
});

//Maincategory factory
$factory->define(App\Maincategory::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
    ];
});