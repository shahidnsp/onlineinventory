<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('barcode');
            $table->string('name');
            $table->integer('unit_id');
            $table->string('discription');
            $table->string('size');
            $table->integer('tax_id');
            $table->integer('subcategory_id');
            $table->integer('maincatecory_id');
            $table->string('color');
            $table->integer('vendor_id');
            $table->string('isactive');
            $table->string('type');
            $table->integer('photo_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
