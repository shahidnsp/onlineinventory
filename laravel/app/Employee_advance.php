<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_advance extends Model
{
    protected $table = 'employee_advances';
    protected $fillable = [ 'employee_id', 'ledger_id', 'amount', 'description', 'paymentdate' ];
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function ledger(){
        return $this->belongsTo('App\Ledger');
    }
}
