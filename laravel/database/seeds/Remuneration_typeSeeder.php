<?php

use Illuminate\Database\Seeder;

class Remuneration_typeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $remuneration_type=factory(App\Remuneration_type::class,10)->create();
    }
}
