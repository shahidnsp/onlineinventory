<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms_content extends Model
{
    protected $fillable = ['content'];
}
