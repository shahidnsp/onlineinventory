<?php

namespace App\Http\Controllers\Auth;

use App\Unit;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class UnitController extends Controller
{
    /**
     * Validate given request
     * @param array $data
     * @return mixed
     *
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'isunit'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Unit::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $unit = new Unit($request->all());
        if($unit->save()){
            return $unit;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $unit= Unit::find($id);
        $unit->fill($request->all());
        if($unit->save()){
            return $unit;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Unit model for cascade delete
        if(Unit::destroy($id))
            return Response::json(array('msg'=>'Unit deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
