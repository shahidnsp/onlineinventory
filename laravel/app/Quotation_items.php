<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation_items extends Model
{
    protected  $fillable =['quotation_id','item_id','rate','discount','qty','unit_id','tax_id','net','mrp'];

    public function quotation(){
        return $this->belongsTo('App\Quotation');
    }

    public function item(){
        return $this->belongsTo('App\Item');
    }

    public function unit(){
        return $this->belongsTo('App\Unit');
    }

    public function tax(){
        return $this->belongsTo('App\Taxe','tax_id');
    }
}
