<?php

use Illuminate\Database\Seeder;

class Purchaseinvoice_itemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchaseinvoice_item=factory(App\Purchaseinvoice_item::class,10)->create();
    }
}
