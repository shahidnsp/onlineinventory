<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['name'];

    public function employee()
    {
        return $this->hasOne('App\Employee');
    }
    public function item()
    {
        return $this->hasOne('App\Item');
    }
    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function contact()
    {
        return $this->hasOne('App\Contact','photo_id');
    }

    public function vender()
    {
        return $this->hasOne('App/Photo');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer');
    }
    public function company_profile()
    {
        return $this->hasOne('App\Company_profile');
    }
}
