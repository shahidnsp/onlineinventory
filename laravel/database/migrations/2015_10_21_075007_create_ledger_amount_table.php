<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgerAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledger_amounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ledger_id');
            $table->string('amount');
            $table->string('opendate');
            $table->string('closedate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ledger_amounts');
    }
}
