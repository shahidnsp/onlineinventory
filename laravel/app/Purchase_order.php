<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_order extends Model
{
    protected $fillable =['vender_id','grossamount','discount','totaltax','netamount','cashdiscount','additional',
        'totalamount','paid','balance','invoicedate','deliverydate','note','ordernote'];

    public function vender()
    {
        return $this->belongsTo('App\Vender');
    }

    public function purchaceordeitems()
    {
        return $this->hasMany('App\Purchaseorder_items');
    }
}
