<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory_item extends Model
{
    protected $fillable=['item_id','expirydate','batchnumber','packetnumber','location_id','quantity','warningqty'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
    public function location()
    {
        return $this->belongsTo('App\Location');
    }
    public function price()
    {
        return $this->hasMany('App\Item_price','inventory_id');
    }
}
