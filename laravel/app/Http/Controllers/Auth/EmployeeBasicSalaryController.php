<?php

/**
 * @author Noushid
 */
namespace App\Http\Controllers\Auth;

use App\Employee_basicsalary;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class EmployeeBasicSalaryController extends Controller
{
    /**
     * validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'employee_id' => 'required',
            'salary' => 'required',
            'effectdate' => 'required'
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Employee_basicsalary::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this -> validator($request->all());
        if($validator->fails())
            return Response::json($validator -> errors(),500);

        $empBscSlry = new Employee_basicsalary($request -> all());

        if($empBscSlry -> save())
            return $empBscSlry;

        return Response::json(['error' => 'Server down!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $empBscSlr = Employee_basicsalary::find($id);
        if($empBscSlr != null)
            return $empBscSlr;
        else
            return Response::json(['error' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors(), 500);

        $basicSlr = Employee_basicsalary::find($id);

        if($basicSlr != null){
            $basicSlr ->fill($request->all());
            if($basicSlr->save())
                return $basicSlr;
            else
                return Response::json(['error', 'Server Down!']);
        }else
            return Response::json(['error' => 'Record not found']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if(Employee_basicsalary::destroy($id))
           return Response::json(['msg' => 'Record deleted']);
        else
           return Response::json(['error' => 'Record not found']);

    }
}
