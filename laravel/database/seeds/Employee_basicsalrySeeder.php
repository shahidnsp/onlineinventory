<?php

use Illuminate\Database\Seeder;

class Employee_basicsalrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee_basicsalary=factory(App\Employee_basicsalary::class,10)->create();
    }
}
