<?php

use Illuminate\Database\Seeder;

class Inventory_itemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inventory_item=factory(App\Inventory_item::class,10)->create();
    }
}
