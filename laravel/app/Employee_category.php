<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_category extends Model
{
    protected $table='employee_categorys';

    public function employee(){
        return $this->hasMany('App\Employee','category_id');
    }
}
