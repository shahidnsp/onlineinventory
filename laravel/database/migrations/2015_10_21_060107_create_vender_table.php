<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('suppliercode');
            $table->string('name');
            $table->string('shortname');
            $table->string('address1');
            $table->string('address2');
            $table->string('city');
            $table->string('pincode');
            $table->string('phone');
            $table->string('mobile');
            $table->string('email');
            $table->string('website');
            $table->string('openbalance');
            $table->string('closingbalance');
            $table->string('discount');
            $table->string('creditlimit');
            $table->string('overduedays');
            $table->string('cstno');
            $table->string('bstno');
            $table->string('kgst');
            $table->string('regino');
            $table->integer('photo_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('venders');
    }
}
