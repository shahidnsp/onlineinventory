<?php
/**
 * @author Noushid
 */
namespace App\Http\Controllers\Auth;

use App\Leavetype;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class EmployeeLeaveTypeController extends Controller
{
    /**
     * validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
         return Validator::make($data,[
             'name' => 'required',
             'alloweddays' => 'required'
         ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Leavetype::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());
        $empLeaveType = new Leavetype($request->all());
        if($empLeaveType->save())
            return $empLeaveType;
        return Response::json(['error' => 'Server down']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empLeaveType = Leavetype::find($id);
        if($empLeaveType != null)
            return $empLeaveType;
        return Response::json(['error', 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());
        $empLeaveType = Leavetype::find($id);
        if($empLeaveType != null)
        {
            $empLeaveType->fill($request->all());
            if($empLeaveType->save())
                return $empLeaveType;
            else
                return Response::json(['error' => 'Server Error']);
        }
        else
            return Response::json(['error' => 'Record not found']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if(Leavetype::destroy($id))
           return Response::json(['msg' => 'Record deleted!']);
       return Response::json(['error' => 'Record Not found!']);

    }
}
