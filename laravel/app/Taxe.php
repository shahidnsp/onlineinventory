<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxe extends Model
{
    protected  $fillable=['name','percentage'];

    public function item()
    {
        return $this->hasMany('App\Item','tax_id');
    }

    public function quotation_item(){
        return $this->hasMany('App\Quotation_items','tax_id');
    }

    public function purchaceorderitems()
    {
        return $this->hasMany('App\purchaceorder_item');
    }

    public function salesinvoiceitem()
    {
        return $this->hasMany('App\Salesinvoice');
    }
}
