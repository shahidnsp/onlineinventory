<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_qualification extends Model
{
    protected $fillable = ['employee_id', 'education', 'college', 'mark'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
