<?php
/**
 * @author Noushid
 */
namespace App\Http\Controllers\Auth;

use App\Remuneration;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class RemunarationController extends Controller
{

    /**
     * validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'remunarationtype_id'  => 'required',
            'ledger_id' => 'required',
            'employee_id' => 'required',
            'name' => 'required',
            'amount' => 'required',
            'paymentdate' => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Remuneration::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());

        $remunaration = new Remuneration($request->all());
        if($remunaration->save())
            return $remunaration;

        return Response::json(['error' => 'Server down!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $remunaration = Remuneration::find($id);
        if($remunaration != null)
            return $remunaration;
        return Response::json(['error' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());
        $remunaration = Remuneration::find($id);
        if($remunaration != null)
        {
            $remunaration->fill($request->all());
            if($remunaration->save())
                return $remunaration;
            else
                return Response::json(['error' => 'Server down!']);
        }else
            return Response::json(['error' => 'Record not found']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Remuneration::destroy($id))
            return Response::json(['msg' => 'Record deleted!']);
        return Response::json(['error' => 'Record not found!']);

    }
}
