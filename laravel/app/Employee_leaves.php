<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_leaves extends Model
{
    protected $fillable = ['employee_id', 'leavetype_id', 'leavedate', 'time'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
    public function type()
    {
        return $this->belongsTo('App\Leavetype','leavetype_id');
    }
}
