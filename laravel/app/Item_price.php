<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_price extends Model
{
    protected $fillable=['inventory_id','salesrate','purchaserate','discount','effectdate'];

    public function inventory()
    {
        return $this->belongsTo('App\Inventory_item');
    }
}
