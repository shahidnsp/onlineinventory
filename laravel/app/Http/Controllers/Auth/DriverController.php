<?php
/**
 * @author Noushid
 */
namespace App\Http\Controllers\Auth;

use App\Driver;
use Illuminate\Http\Request as request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class DriverController extends Controller
{

    /**
     * validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'mobile' => 'required',
            'vehicle_id' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Driver::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());

        $driver = new Driver($request->all());
        if($driver->save())
            return $driver;

        return Response::json(['error' => 'Server down']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $driver = Driver::find($id);
        if($driver != null)
            return $driver;
        return Response::json(['error' => 'record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
            return Response::json($validator->errors());
        $driver = Driver::find($id);
        if($driver != null)
        {
            $driver->fill($request->all());
            if($driver->save())
                return $driver;
            else
                return Response::json(['error' => 'Server down!']);
        }else
            return Response::json(['error' => 'Record not found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Driver::destroy($id))
            return Response::json(['msg' => 'Record deleted!']);
        return Response::json(['error' => 'Record not found!']);
    }
}
