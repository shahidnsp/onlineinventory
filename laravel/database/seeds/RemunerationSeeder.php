<?php

use Illuminate\Database\Seeder;

class RemunerationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $remuneration=factory(App\Remuneration::class,10)->create();
    }
}
