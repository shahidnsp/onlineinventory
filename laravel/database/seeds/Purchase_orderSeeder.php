<?php

use Illuminate\Database\Seeder;

class Purchase_orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchase_order=factory(App\Purchase_order::class,10)->create();
    }
}
