<?php

use Illuminate\Database\Seeder;

class Salesinvoice_itemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salesinvoiceitem=factory(App\Salesinvoice_item::class,10)->create();
    }
}
