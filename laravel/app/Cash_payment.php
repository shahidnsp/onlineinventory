<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash_payment extends Model
{  protected $fillable = ['ledger_id','date','account_id','amount','cashdiscount','narration'];
    public function account()
    {
        return $this->belongsTo('App\Account');
    }
    public function ledger()
    {
        return $this->belongsTo('App\Ledger');
    }

}
