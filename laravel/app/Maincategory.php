<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maincategory extends Model
{
    protected $fillable=['name'];

    public function item()
    {
        return $this->hasMany('App\Item','maincatecory_id');
    }

    public function subcategory(){
        return $this->hasMany('App\Subcategory');
    }
}
