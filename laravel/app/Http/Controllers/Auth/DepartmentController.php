<?php

namespace App\Http\Controllers\Auth;

use App\Department;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

/**
 * Class DepartmentController
 * @package App\Http\Controllers\Auth
 * @author Noushid
 */
class DepartmentController extends Controller
{
    /**
     * validate the given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, ['name' => 'required', 'hod' => 'required']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Department::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors(), 400);
        }

        $department = new Department($request->all());
        if($department->save()){
            return $department;
        }

        return Response::json(['error' => 'server down']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::find($id);
        if($department != null)
            return $department;
        else
            return Response::json(['error' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){

            return Response::json($validator->errors(), 400);
        }

        $department = Department::find($id);
        if($department != null){
            $department -> fill($request -> all());
            if($department -> save())
                return $department;
        }else{
            return Response::json(['error' => 'Record not found']);
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Department model for cascade delete
        if(Department::destroy($id))
            return Response::json(['msg' => 'Record deleted']);
        else
            return Response::json(['error'=> 'Record not found'], 400);

    }
}
