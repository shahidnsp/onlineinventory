<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $fillable = [
                            'paymentdate', 'debitaccount', 'creditaccount',
                            'debitamount', 'creditamount', 'ledgerfolio',
                            'description','vouchertype','sales','purchase'
                         ];


}
