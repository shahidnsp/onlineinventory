<?php

namespace App\Http\Controllers\Auth;


use App\Department;
use App\Employee;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

/**
 * @author Noushid
 * Class EmployeeController
 * @package App\Http\Controllers\Auth
 */
class EmployeeController extends Controller
{

    /**
     * validate given request
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'empcode' => 'required',
            'category_id' => 'required',
            'name' => 'required',
            'department' => 'required',
            'designation' =>   'required'
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request ->all());
        if($validator->fails()){

            return Response::json($validator->errors(), 400);
        }

        $employee = new Employee($request->all());
        if($employee->save()){
            return $employee;
        }
        return Response::json(['error' => 'sorry ! Server Error'],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Employee::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){

            return Response::json($validator -> errors(), 400);
        }

        $employee = Employee::find($id);

        if($employee != null){
            $employee -> fill($request->all());
            if($employee -> save())
                return $employee;
        }else{
            return Response::json(['error' => 'No record found']);
        }

        return Response::json(['error' => 'Server is down'], 500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Employee model for cascade delete

        if(Department::destroy($id))
            return Response::json(['msg' => 'Record deleted']);
        else
            return Response::json(['error' => 'Record not found']);
    }
}
