<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remuneration extends Model
{
    protected $fillable = ['remunarationtype_id', 'ledger_id', 'employee_id', 'name', 'amount', 'description', 'paymentdate'];
   public function employee()
   {
       return $this->belongsTo('App\Employee');
   }
    public function remunarationtype()
    {
        return $this->belongsTo('App\Remuneration_type');
    }
    public function ledger(){
        return $this->belongsTo('App\Ledger');
    }
}
