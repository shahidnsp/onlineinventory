<?php

namespace App\Http\Controllers\Auth;

use App\Bank_receipt;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class BankReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator($data)
    {
        return validator::make($data,[
            'account_id'=>'required',
            'datebanked'=>'required',
        ]);
    }
    public function index()
    {
        return Bank_receipt::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $receipt = new Bank_receipt($request->all());
        $receipt->fill($request->all());
        if($receipt->save())
        {
            return $receipt;
        }
        else
            return Response::json( ['error' => 'Server is down'],500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $reciept = Bank_receipt::find($id);
        if($receipt != null)
        {
            $reciept->fill($request->all());
            if($reciept->save()){
                return $reciept;
            }
            return Response::json( ['error' => 'Server is down']
            ,500);
        }
        else
            return Response::json(['error'=>'Record not found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Bank_receipt::destroy($id))
            return Response::json(array('msg'=>'Banks Account deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
