<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->string('address');
            $table->string('tin');
            $table->string('cst');
            $table->string('servicetax');
            $table->string('city');
            $table->string('pincode');
            $table->string('mobile');
            $table->string('phone');
            $table->string('email');
            $table->string('website');
            $table->string('openbalance');
            $table->string('closingbalance');
            $table->string('creditlimit');
            $table->string('creditlimitday');
            $table->integer('photo_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
