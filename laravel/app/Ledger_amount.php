<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledger_amount extends Model
{
    protected $table='ledger_amounts';
    protected $fillable = ['ledger_id','amount','opendate','closedate'];
    public function ledger()
    {
        return $this->belongsTo('App\Ledger');
    }
}
