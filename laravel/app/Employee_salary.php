<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_salary extends Model
{

    protected $table = 'employee_salarys';
    protected $fillable = ['employee_id','presentday', 'education', 'overtime', 'overtimeamount', 'paymentdate ', 'paymentmode', 'overtimerate', 'description', 'total'];

   public function employee()
   {
       return $this->belongsTo('App\Employee');
   }
}
