<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchaseorder_items extends Model
{
    Protected $fillable=['purchaseorder_id','item_id','rate','discount','qty','unit_id','tax_id','net','mrp'];

    public function purchaceorder()
    {
        return $this->belongsTo('App\Purchase_order');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function tax()
    {
        return $this->belongsTo('App\Tax');
    }
}
