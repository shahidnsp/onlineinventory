<?php

/**
 * @author noushid
 */

namespace App\Http\Controllers\Auth;

use App\Employee_experiance;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class EmployeeExperianceController extends Controller
{

    /**
     * validate the given request
     * @param array $data
     * @return mixed
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'employee_id' => 'required',
            'designation' => 'required'
        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee_experiance::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator -> errors(), 500);
        }

        $employeeExperience = new Employee_experiance($request -> all());
        if($employeeExperience->save())
            return $employeeExperience;

        return Response::json(['error' => 'Server down!'], 500);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Employee_experiance::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
            return Response::json($validator->errors(), 400);

        $employeeExperience = Employee_experiance::find($id);

        if($employeeExperience!=null) {
            $employeeExperience->fill($request->all());

            if ($employeeExperience->save())
                return $employeeExperience;
        }else{
            return Response::json(['error' => 'No record find'], 500);
        }

        return Response::json(['error' => 'Server is down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO employeeExperiance model for cascade delete

        if(Employee_experiance::destroy($id))
            return Response::json(['msg' => 'Record Deleted']);
        else
            return Response::json(['error' => 'record not found']);
    }
}
