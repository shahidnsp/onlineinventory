<?php

namespace App\Http\Controllers\Auth;

use App\Sms_content;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class SmsContentController extends Controller
{
    /**
     * Validate given request
     * @param array $data
     * @return mixed
     *
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'content'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Sms_content::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $content = new Sms_content($request->all());
        if($content->save()){
            return $content;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $content = Sms_content::find($id);
        $content->fill($request->all());
        if($content->save()){
            return $content;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Sms_content model for cascade delete
         if(Sms_content::destroy($id))
             return Response::json(array('msg'=>'SMS Content deleted'));
         else
             return Response::json(array('error'=>'Records not found'),400);
    }
}
