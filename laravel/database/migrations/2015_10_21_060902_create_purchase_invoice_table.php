<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoiceno');
            $table->string('invoicedate');
            $table->string('deliverydate');
            $table->integer('ledger_id');
            $table->integer('vender_id');
            $table->string('grossamount');
            $table->string('discount');
            $table->string('totaltax');
            $table->string('netamount');
            $table->string('cashdiscount');
            $table->string('freightcharge');
            $table->string('additional');
            $table->string('givenamount');
            $table->string('balance');
            $table->string('balancepaiddate');
            $table->string('note');
            $table->string('invoicenote');
            $table->string('totalamount');
            $table->string('paymentmode');
            $table->integer('glaccount_id');
            $table->string('creditnotification');
            $table->string('notiactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_invoices');
    }
}
