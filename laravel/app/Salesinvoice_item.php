<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesinvoice_item extends Model
{
    protected $fillable=['salesinvoice_id', 'item_id','rate','discount','qty','unit_id','tax_id','net','mrp'];


    public function selesinvoice()
    {
        return $this->belongsTo('App\Salesinvoice');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function tax()
    {
        return $this->belongsTo('App\Tax');
    }

}



